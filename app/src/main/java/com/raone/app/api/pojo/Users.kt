package com.raone.app.api.pojo

data class Users(
    val id: Int,
    val name: String,
    val username: String,
    val email: String
)