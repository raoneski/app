package com.raone.app

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.raone.app.di.applicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {

    override fun onCreate() {
        super.onCreate()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        instance = this
        setup()
    }

    private fun setup() {
        startKoin {
            androidContext(this@App)
            modules(listOf(applicationModule))
        }
    }

    companion object {
        lateinit var instance: App private set
    }
}