package com.raone.app.di

import android.content.Context
import android.content.SharedPreferences
import com.raone.app.interactor.JsonInteractorImpl
import com.raone.app.interactor.SettingsInteractor
import com.raone.app.manager.SettingsManager
import com.raone.app.presenter.SettingsPresenter
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val applicationModule = module (override = true) {
    single<SharedPreferences> { androidContext().getSharedPreferences("DhwAHWIUHFdjwiHWUIhudiWIUANSAI:r1", Context.MODE_PRIVATE) }
    single<SettingsManager> { SettingsManager(get()) }
    single<SettingsInteractor> { SettingsInteractor(get()) }
    single { SettingsPresenter(get()) }

    single { JsonInteractorImpl() }
}