package com.raone.app.presenter

import android.util.Log
import com.raone.app.base.BasePresenter
import com.raone.app.interactor.JsonInteractorImpl
import com.raone.app.view.IJson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class JsonPresenter(): BasePresenter<IJson>() {
    val interactorImpl = JsonInteractorImpl()

    fun getUsers() {
        mainView?.showProgressBar()

        disposable.add(
            interactorImpl.getUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    mainView?.hideProgressBar()
                    mainView?.showUsers(it)
                    Log.i("mainLogs", "JsonPresenter: getUsers, SUCCESS - $it")
                }, {
                    mainView?.hideProgressBar()
                    mainView?.showError("$it")
                    Log.i("mainLogs", "JsonPresenter: getUsers, ERROR - $it")
                })
        )
    }
}