package com.raone.app.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import com.raone.app.R
import com.raone.app.interactor.SettingsInteractor
import com.raone.app.presenter.SettingsPresenter
import com.raone.app.ui.fragments.UsersFragment
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class MainActivity : AppCompatActivity(){
    private var currentFramgnet = -1
    var settingsPresenter: SettingsPresenter = get()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addFragmentWithStack(UsersFragment.newInstance(), "UsersFragment")
    }

    override fun onBackPressed() {
        super.onBackPressed()

    }

    private fun addFragmentWithStack(fragment: Fragment, tag: String) {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.flMainContainer, fragment, tag)
            .addToBackStack(tag)
            .commit()
    }

    private fun replaceFragmentWithStack(fragment: Fragment, tag: String) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.flMainContainer, fragment, tag)
            .addToBackStack(tag)
            .commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        supportFragmentManager.fragments.forEach { it.onActivityResult(requestCode, resultCode, data) }
    }

    companion object {
    }
}
