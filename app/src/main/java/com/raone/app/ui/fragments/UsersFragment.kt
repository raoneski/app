package com.raone.app.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.raone.app.R
import com.raone.app.api.pojo.Users
import com.raone.app.presenter.JsonPresenter
import com.raone.app.ui.adapter.UsersAdapter
import com.raone.app.view.IJson
import kotlinx.android.synthetic.main.fragment_users.*


class UsersFragment : Fragment(), IJson {

    private val jsonPresenter = JsonPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_users, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        jsonPresenter.onAttach(this)
        jsonPresenter.getUsers()
    }

    companion object {
        @JvmStatic
        fun newInstance() = UsersFragment()
    }

    override fun showError(message: String) {

    }

    override fun showProgressBar() {
        pbUsers?.visibility = View.VISIBLE
        rvUsers?.visibility = View.GONE
    }

    override fun hideProgressBar() {
        pbUsers?.visibility = View.GONE
    }

    override fun showUsers(users: List<Users>) {
        rvUsers?.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = UsersAdapter(users)
        }

        rvUsers?.visibility = View.VISIBLE
    }
}