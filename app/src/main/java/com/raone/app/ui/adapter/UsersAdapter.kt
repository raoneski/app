package com.raone.app.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.raone.app.R
import com.raone.app.api.pojo.Users
import kotlinx.android.synthetic.main.item_user.view.*

class UsersAdapter (val dataSet: List<Users>): RecyclerView.Adapter<UsersAdapter.ViewHolder> () {

    val materialColors = listOf<Int>(R.color.material_ava_1, R.color.material_ava_2, R.color.material_ava_3, R.color.material_ava_4, R.color.material_ava_5, R.color.material_ava_6)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false))
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setData(dataSet[position])
    }

    inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        fun setData(user: Users) {
            view?.tvName?.text = user.name
            view?.tvEmail?.text = user.email
            view?.tvUserAva?.text = user.name[0].toString()
            view?.tvUserAva.background.setTint(view.resources.getColor(materialColors[(0 until 6).random()]))
        }
    }
}