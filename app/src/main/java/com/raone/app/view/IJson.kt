package com.raone.app.view

import com.raone.app.api.pojo.Users

interface IJson {
    fun showError(message: String)
    fun showProgressBar()
    fun hideProgressBar()

    fun showUsers(users: List<Users>)
}