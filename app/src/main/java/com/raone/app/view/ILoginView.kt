package com.raone.app.view

interface ILoginView {
    fun loading()
    fun hideLoading()
    fun success()
    fun error(messge: String)
}