package com.raone.app.interactor

import com.raone.app.api.CoreApi
import com.raone.app.api.pojo.Users
import io.reactivex.Single

class JsonInteractorImpl(): JsonInteractor {
    override fun getUsers(): Single<List<Users>> {
        return CoreApi.instance.getUsers()
    }
}

interface JsonInteractor {
    fun getUsers(): Single<List<Users>>

}
