package com.raone.app.base

import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<T> : Presenter() {
    var mainView: T? = null
    var disposable = CompositeDisposable()

    open fun onAttach(view: T){
        mainView = view
        disposable = CompositeDisposable()
    }

    open fun onDetach(){
        mainView = null
        disposable.dispose()
    }
}

abstract class Presenter